package com.gitlab.fisvse.frantisekstrba.adventura.main;

public interface Observer {
    /**
     * aktualizuje stav
     */
    void update();
}

