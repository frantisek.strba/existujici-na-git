package com.gitlab.fisvse.frantisekstrba.adventura.logika;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.util.HashMap;
import java.util.Map;

/**
 * Třída Vec reprezentuje předmět na mapě.
 * <p>
 * Tato třída je součástí jednoduché textové hry.
 *
 * @author Frantisek Strba
 * @version 1.0
 * @created ZS 2018
 */
public class Vec extends ImageView {

    private final String jmeno;
    private final String popis;
    private final Map<String, Vec> seznamVeci;
    private boolean prenositelna;
    private String otazka;
    private String odpoved;
    private boolean prozkoumana;
    private boolean otevrena;
    private String imagePath;

    /**
     * *************************************************************************
     * Konstruktor. Předmět může být přenositelný, má popis, prozkoumané
     * předměty mohou vydat předměty v sobě. Některé předměty mohou být zavřené.
     *
     * @param jmeno
     * @param prenositelna
     * @param popis
     */
    public Vec(String jmeno, boolean prenositelna, String popis) {
        this.jmeno = jmeno;
        this.prenositelna = prenositelna;
        this.popis = popis;
        this.imagePath = imagePath;
        prozkoumana = false;
        otevrena = true;
        seznamVeci = new HashMap<>();


        if (isPrenositelna()) {
            Image obrazek = new Image(com.gitlab.fisvse.frantisekstrba.adventura.main.Start.class.getResourceAsStream
                    ("/images/" + jmeno + ".png"), 50, 50, false, false);
            this.setImage(obrazek);
        }
    }

    /**
     * Vrací jméno věci
     *
     * @return
     */
    public String getJmeno() {
        return jmeno;
    }

    /**
     * Vrací přenositelnost věci
     *
     * @return
     */
    public boolean isPrenositelna() {
        return prenositelna;
    }

    public void setPrenositelna(boolean prenositelna) {
        this.prenositelna = prenositelna;
    }

    public String getPopis() {
        return popis;
    }

    public void nastavOtazku(String otazka, String odpoved) {
        this.otazka = otazka;
        this.odpoved = odpoved;
    }

    /**
     * Metoda vrací otázku příslušnou dané věci s otázkou.
     * <p>
     * *@return String otázka.
     */
    public String getOtazka() {
        return otazka;
    }

    /**
     * Metoda vrací odpověď příslušnou dané věci s otázkou.
     * <p>
     * *@return String odpověď
     */
    public String getOdpoved() {
        return odpoved;
    }

    /**
     * Metoda na zjištění zda je pro otevření(prozkoumání věci potřeba
     * zodpovědět otázku.
     *
     * @return vrací true, pokud ano.
     */
    public boolean sKodem() {
        return otazka != null;
    }

    public boolean isProzkoumana() {
        return prozkoumana;
    }

    public void setProzkoumana(boolean jeProzkoumana) {
        this.prozkoumana = jeProzkoumana;
    }

    /**
     * Vloží věc do jiné věci (trezor).
     *
     * @param vec
     */
    public void vlozVec(Vec vec) {
        seznamVeci.put(vec.getJmeno(), vec);
    }

    /**
     * Zjistí, zda se daná věc nachází v jiné věci.
     *
     * @param jmeno
     * @return vrátí true, pokud je věc ve věci.
     */
    public boolean obsahujeVec(String jmeno) {
        return prozkoumana && seznamVeci.containsKey(jmeno);
    }

    /**
     * Vybere věc z jiné prozkoumané věci.
     *
     * @param jmeno
     * @return vrátí vybranou věc
     */
    public Vec vyberVec(String jmeno) {
        Vec vec = null;
        if (prozkoumana && seznamVeci.containsKey(jmeno)) {
            vec = seznamVeci.get(jmeno);
            if (vec.isPrenositelna()) {
                seznamVeci.remove(jmeno);
            }
        }
        return vec;
    }

    /**
     * Při prozkoumání vrací popis dané věci, případně s věcmi, které obsahuje.
     *
     * @return popis
     */
    public String popisProzkoumej() {
        if (seznamVeci.isEmpty()) {
            return popis;
        }

        String textKVypsani = "Prohlédl jsi si důkladně " + jmeno + " a našel jsi:";
        for (String jmenoVeciUvnitr : seznamVeci.keySet()) {
            textKVypsani += " " + jmenoVeciUvnitr;
        }
        return textKVypsani;
    }

    /**
     * Vrací odkaz na seznam věcí, které daná věc obsahuje.
     *
     * @return seznamVeci
     */
    public String getSeznamVeci() {
        String nazvy = " ";
        if (prozkoumana = true) {
            nazvy = seznamVeci.keySet().stream().map((jmenoVeci) -> jmenoVeci + " ").reduce(nazvy, String::concat);
        }
        return nazvy;
    }

    public boolean isOtevrena() {
        return otevrena;
    }

    /**
     * Metoda pro nastavení, zda je věc otevřená-true, pokud je-pak ji lze
     * rovnou prozkoumat, jinak nutno napřed otevřít.
     *
     * @param otevrena
     */
    public void setOtevrena(boolean otevrena) {
        this.otevrena = otevrena;
    }

    public String getImagePath() {
        return imagePath;
    }
}
