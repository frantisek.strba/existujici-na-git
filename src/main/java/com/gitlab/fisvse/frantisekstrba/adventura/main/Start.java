/* Soubor je ulozen v kodovani UTF-8.
 * Kontrola kódování: Příliš žluťoučký kůň úpěl ďábelské ódy. */
package com.gitlab.fisvse.frantisekstrba.adventura.main;


import com.gitlab.fisvse.frantisekstrba.adventura.logika.Hra;
import com.gitlab.fisvse.frantisekstrba.adventura.logika.IHra;
import com.gitlab.fisvse.frantisekstrba.adventura.uiText.TextoveRozhrani;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

/*******************************************************************************
 * Třída  Start je hlavní třídou projektu,
 * který představuje jednoduchou textovou adventuru určenou k dalším úpravám a rozšiřování
 *
 * @author Jarmila Pavlíčková
 * @version ZS 2016/2017
 */
public class Start extends Application {
    /***************************************************************************
     * Metoda, prostřednictvím níž se spouští celá aplikace.
     *
     * @param args Parametry příkazového řádku
     */
    public static void main(String[] args) {
        if (args.length > 0) {
            if (args[0].equals("text")) {
                IHra hra = new Hra();
                TextoveRozhrani ui = new TextoveRozhrani(hra);
                ui.hraj();
            } else {
                System.out.println("Neplatný parametr. Zadejte parametr text pro textovou verzi.");
            }
        } else {
            launch(args);
        }

    }

    /**
     * Vytvoří se nové okno a inicializuje se nová hra.
     *
     * @param primaryStage hlavní okno
     * @throws Exception
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/main.fxml"));
        GridPane root = loader.load();
        MainController controller = loader.getController();


        primaryStage.setTitle("Adventura");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();

        IHra hra = new Hra();
        controller.inicializuj(hra);
    }

}
