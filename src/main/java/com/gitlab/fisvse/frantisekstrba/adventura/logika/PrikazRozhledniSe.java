package com.gitlab.fisvse.frantisekstrba.adventura.logika;

/**
 * Třída
 * <p>
 * Tato třída je součástí jednoduché textové hry.
 *
 * @author Frantisek Strba
 * @version 1.0
 * @created ZS 2018
 */
class PrikazRozhledniSe implements IPrikaz {

    private static final String NAZEV = "rozhledni";
    private final HerniPlan plan;

    public PrikazRozhledniSe(HerniPlan plan) {
        this.plan = plan;

    }

    /**
     * Vrací předměty v místnosti. Pokud místnost obsahuje předměty nebo
     * postavy, příkaz je vypíše jako text.
     *
     * @return napoveda ke hre
     */
    @Override
    public String provedPrikaz(String... parametry) {
        return plan.getAktualniProstor().predmetyVMistnosti();
    }

    /**
     * Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *
     * @ return nazev prikazu
     */
    @Override
    public String getNazev() {
        return NAZEV;
    }

}
