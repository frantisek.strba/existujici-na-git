package com.gitlab.fisvse.frantisekstrba.adventura.logika;

import com.gitlab.fisvse.frantisekstrba.adventura.main.Observer;
import com.gitlab.fisvse.frantisekstrba.adventura.main.Subject;

import java.util.ArrayList;
import java.util.List;


/**
 * Class HerniPlan - třída představující mapu a stav adventury.
 * <p>
 * Tato třída inicializuje prvky ze kterých se hra skládá: vytváří všechny
 * prostory, propojuje je vzájemně pomocí východů a pamatuje si aktuální
 * prostor, ve kterém se hráč právě nachází.
 */
public class HerniPlan implements Subject {

    private final List<Prostor> prostory;
    private final Inventar inventar;
    private Prostor aktualniProstor;
    private List<Observer> seznamPozorovatelu;

    /**
     * Konstruktor který vytváří jednotlivé prostory a propojuje je pomocí
     * východů.
     */
    public HerniPlan() {
        prostory = new ArrayList<>();
        zalozProstoryHry();
        inventar = new Inventar();

        seznamPozorovatelu = new ArrayList<>();
    }

    /**
     * Vytváří jednotlivé prostory a propojuje je pomocí východů. Jako výchozí
     * aktuální prostor nastaví Řídící místnost.
     */
    private void zalozProstoryHry() {
        // vytvářejí se jednotlivé prostory

        Prostor ridiciMistnost = new Prostor("ridici_mistnost", "Řídící místnost. Víš, že se v místnosti nachází řídící počítač. Stálo by za to ho prozkoumat.");
        Prostor spolecenskaMistnost = new Prostor("spolecenska_mistnost", "Společenská místnost. Vidíš mrtvá těla tvých přátel, všechno je zničené..");
        Prostor chodbaA = new Prostor("chodba_A", "Chodba A. Nic zajímavého zde nevidíš.");
        Prostor chodbaB = new Prostor("chodba_B", "Chodba B. Nic zajímavého zde nevidíš");
        Prostor temnaMistnost = new Prostor("temna_mistnost", "Temná místnost. V této místnosti není vidět. Abys mohl jít dále, musíš najít a použít nějaký zdroj světla");
        Prostor mistnostTrezor = new Prostor("mistnost_trezor", "Místnost s trezorem. V této místnosti je trezor, který by mohl obsahovat zajímavé věci.");
        Prostor skladZasob = new Prostor("sklad_zasob", "Sklad zásob. Všude kolem jsou různé předměty. Některé by se mohly hodit, ale pozor! Neuneseš všechno.");
        Prostor skladZbrane = new Prostor("sklad_zbrane", "Sklad zbraní. Nějaká zbraň na boj by se mohla hodit.");
        Prostor chodbaLeva = new Prostor("chodba_leva", "Levá chodba. Cítíš mrazivý pocit, víš že tě v další místnosti čeká velké nebezpečí. Měl bys být připraven.");
        Prostor chodbaPrava = new Prostor("chodba_prava", "Pravá chodba. Cítíš mrazivý pocit, víš že tě v další místnosti čeká velké nebezpečí. Měl bys být připraven.");
        Prostor cestaVen = new Prostor("cesta_ven", "Cesta ven. Objevil se před tebou nebezpečný mimozemšťan. Abys mohl projít, musíš použít svojí zbraň.");
        Prostor exit = new Prostor("exit", "Jsi venku.");

        prostory.add(ridiciMistnost);
        prostory.add(spolecenskaMistnost);
        prostory.add(chodbaA);
        prostory.add(chodbaB);
        prostory.add(temnaMistnost);
        prostory.add(mistnostTrezor);
        prostory.add(skladZasob);
        prostory.add(skladZbrane);
        prostory.add(chodbaLeva);
        prostory.add(chodbaPrava);
        prostory.add(cestaVen);
        prostory.add(exit);

        // Propojení jednotlivých prostor
        ridiciMistnost.setVychod(spolecenskaMistnost);

        spolecenskaMistnost.setVychod(chodbaA);
        spolecenskaMistnost.setVychod(chodbaB);
        spolecenskaMistnost.setVychod(ridiciMistnost);

        chodbaA.setVychod(temnaMistnost);
        chodbaA.setVychod(spolecenskaMistnost);

        chodbaB.setVychod(mistnostTrezor);
        chodbaB.setVychod(spolecenskaMistnost);

        mistnostTrezor.setVychod(skladZasob);
        mistnostTrezor.setVychod(chodbaPrava);
        mistnostTrezor.setVychod(chodbaB);

        temnaMistnost.setVychod(skladZbrane);
        temnaMistnost.setVychod(chodbaLeva);
        temnaMistnost.setVychod(chodbaA);

        skladZbrane.setVychod(temnaMistnost);

        skladZasob.setVychod(mistnostTrezor);

        chodbaLeva.setVychod(cestaVen);
        chodbaLeva.setVychod(temnaMistnost);

        chodbaPrava.setVychod(cestaVen);
        chodbaPrava.setVychod(mistnostTrezor);

        cestaVen.setVychod(chodbaLeva);
        cestaVen.setVychod(chodbaPrava);
        cestaVen.setVychod(exit);

        // Vytváření itemů a postav v mapě.
        Vec computer = new Vec("ridici_pocitac", false, "Řídící počítač je značně"
                + " poškozen. I tak ale po prozkoumání víš, že si narazil na "
                + "nějaký důležitý kód. Kód: 13 -> BINARY");
        ridiciMistnost.vlozVec(computer);
        ridiciMistnost.vlozVec(new Vec("mrtvola", false, "Leží vedle tebe mrtvola zabitého vojáka"));

        skladZasob.vlozVec(new Vec("baterka", true, "Tato baterka poskytuje cenný"
                + "zdroj světla. Určitě by se mohla hodit"));
        skladZasob.vlozVec(new Vec("hasici_pristroj", true, "Tento hasící "
                + "přístroj může uhasit zdroje požáru. Nejspíš ale nikde nehoří."));
        skladZasob.vlozVec(new Vec("zidle", false, "Na sezení teď není čas."));
        skladZasob.vlozVec(new Vec("oblek", true, "Další oblek uneseš, ale asi k ničemu nebude"));
        skladZasob.vlozVec(new Vec("jidlo", true, "Jídlo by se mohlo hodit, ale měl bys raději rychle uprchnout"));


        skladZbrane.vlozVec(new Vec("pistol", true, "Zbraň by se mohla hodit. Určitě bys jí měl vzít."));
        skladZbrane.vlozVec(new Vec("nuz", true, "Nůž ti asi na mimozemské příšery moc nepomůže"));


        Vec klic = new Vec("kapitansky_klic", true, "Víš, že tento kapitánský klíč odemyká dveře do skladu zbraní.");

        skladZbrane.setZamcena(true);
        skladZbrane.nastavKlic(klic);

        skladZbrane.setVisible(false);
        chodbaLeva.setVisible(false);

        exit.setVisible(false);

        Vec trezor = new Vec("trezor", false, "Trezor je již odemčený a nic užitečného v něm není.");
        trezor.nastavOtazku("Kód do trezoru:", "1101");
        trezor.setOtevrena(false);
        trezor.vlozVec(klic);
        mistnostTrezor.vlozVec(trezor);
        Postava mimozemstan = new Postava("mimozemstan");
        cestaVen.vlozPostavu(mimozemstan);

        aktualniProstor = ridiciMistnost;  // hra začíná v Řídící místnosti.

    }

    /**
     * Metoda vrací odkaz na aktuální prostor, ve ktetém se hráč právě nachází.
     *
     * @return aktuální prostor
     */
    public Prostor getAktualniProstor() {
        return aktualniProstor;
    }

    /**
     * Metoda nastaví aktuální prostor, používá se nejčastěji při přechodu mezi
     * prostory
     *
     * @param prostor nový aktuální prostor
     */
    public void setAktualniProstor(Prostor prostor) {
        aktualniProstor = prostor;
        notifyObservers();
    }

    /**
     * Prostor se stane viditelným
     *
     * @param prostor prostor ke zviditelnění
     */
    public void setProstorVisible(Prostor prostor) {
        prostor.setVisible(true);
    }

    /**
     * vrací prostor podle jména
     * @param name jméno prostoru
     * @return
     */
    public Prostor getProstorByName(String name) {
        for (Prostor prostor : prostory) {
            if (prostor.getNazev().equals(name)) {
                return prostor;
            }
        }
        return null;
    }

    /**
     * vrací Inventář
     * @return inventář
     */
    public Inventar getInventar() {
        return inventar;
    }

    /**
     * registruje observer
     * @param observer
     */
    @Override
    public void register(com.gitlab.fisvse.frantisekstrba.adventura.main.Observer observer) {
        seznamPozorovatelu.add(observer);
    }

    /**
     * odebere observer
     * @param observer
     */
    @Override
    public void unregister(com.gitlab.fisvse.frantisekstrba.adventura.main.Observer observer) {
        seznamPozorovatelu.remove(observer);
    }

    /**
     * oznámení observerům
     */
    @Override
    public void notifyObservers() {
        for (Observer pozorovatel : seznamPozorovatelu) {
            pozorovatel.update();
        }

    }

}
