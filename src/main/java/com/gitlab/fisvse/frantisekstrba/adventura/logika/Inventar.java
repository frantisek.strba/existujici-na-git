package com.gitlab.fisvse.frantisekstrba.adventura.logika;

import java.util.HashMap;
import java.util.Map;

/**
 * Třída Inventar představuje "batoh" hráče.
 * <p>
 * Tato třída je součástí jednoduché textové hry.
 *
 * @author Frantisek Strba
 * @version 1.0
 * @created ZS 2018
 */
public class Inventar {

    /**
     * Volitelná kapacita inventáře.
     */
    private static final int KAPACITA = 3;
    /**
     * Předměty v inventáři.
     */
    private final Map<String, Vec> veci;

    public Inventar() {
        veci = new HashMap<>();
    }

    /**
     * Pokud má inventář dostatečnou kapacitu, přidá předmět do inventáře.
     *
     * @param vec Předmět, co se má přidat
     * @return True pokud předmět úspěšně přidá. False pokud ne.
     */
    public boolean vlozVecDoInventare(Vec vec) {
        if (veci.size() < KAPACITA) {
            veci.put(vec.getJmeno(), vec);
            return true;
        }
        return false;
    }

    /**
     * Vrátí String s věcmi v inventáři, pokud v něm žádné nesjou napíše, že je
     * prázdný.
     *
     * @return
     */
    public String obsahInventare() {
        if (veci.isEmpty()) {
            return "Inventář je prázdný.";
        }
        String veciInventar = "Věci v inventáři: ";
        veciInventar = veci.keySet().stream().map((nazevVeci) -> nazevVeci + " ").reduce(veciInventar, String::concat);
        return veciInventar;
    }

    public int getKapacita() {
        return KAPACITA;
    }

    public Vec odeberVec(String nazev) {
        return veci.remove(nazev);
    }

    /**
     * zjišťuje, zda se určitá věc nachází v inventáři
     *
     * @param nazevVeci která se má nacházet v inventáři
     * @return true pokud inventář obsahuje věc
     */
    public boolean obsahujeVec(String nazevVeci) {
        return veci.containsKey(nazevVeci);
    }

    Vec getVec(String nazev) {
        return veci.get(nazev);
    }


    public Map<String, Vec> getVeci() {
        return veci;
    }

}
