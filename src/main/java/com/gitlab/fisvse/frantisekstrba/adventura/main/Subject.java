package com.gitlab.fisvse.frantisekstrba.adventura.main;

public interface Subject {
    /**
     * zaregistruje pozorovatele
     *
     * @param observer
     */
    void register(Observer observer);

    /**
     * odregistruje pozorovatele
     *
     * @param observer
     */
    void unregister(Observer observer);

    /**
     * oznámení pozorovatelům
     */
    void notifyObservers();

}
