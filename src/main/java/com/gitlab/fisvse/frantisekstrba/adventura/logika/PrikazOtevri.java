package com.gitlab.fisvse.frantisekstrba.adventura.logika;

/**
 * Třída PrikazOtevri realizuje příkaz otevri.
 * <p>
 * Tato třída je součástí jednoduché textové hry.
 *
 * @author Frantisek Strba
 * @version 1.0
 * @created ZS 2018
 */
public class PrikazOtevri implements IPrikaz {

    private static final String NAZEV = "otevri";
    private final HerniPlan plan;
    private TextoveDotazy zadDotazu = null;

    /*
     *Konstruktor třídy PrikazOtevri
     * @param plan
     */
    public PrikazOtevri(HerniPlan plan) {
        this.plan = plan;
        zadDotazu = new TextoveDotazy();
    }

    /**
     * Provádí příkaz "otevri". Zkouší otevřít zamčenou věc s určitou otázkou.      *
     *
     * @param parametry - jako parametr obsahuje jméno věci, která se má
     *                  otevřít.
     * @return zpráva, kterou vypíše hra hráči
     */
    @Override
    public String provedPrikaz(String... parametry) {
        if (parametry.length == 0) {

            return "Co mám otevřít? ";
        }

        String vecString = parametry[0];
        Vec vec = plan.getAktualniProstor().getVec(vecString);

        // zadaná věc neexistuje v této místnosti 
        if (vec == null) {
            return "To tady není!";
        } else {
            if (vec.isOtevrena()) {
                return "Tuto věc není třeba otevírat.";
            }
            String text;
            if (vec.sKodem()) {
                String odpoved = zadDotazu.odpovez(vec.getOtazka());
                if (odpoved.trim().equals(vec.getOdpoved())) {
                    text = "Podařilo se ti zadat správný kód a opatrně otevíráš trezor.\n";
                    vec.setOtevrena(true);
                } else {
                    text = "Trezor nejde otevřít. Asi nemáš ten správný kód.\n";
                }
                return text;
            } else {
                return "Tuto věc není třeba otevírat.";
            }
        }
    }

    /**
     * Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *
     * @ return nazev prikazu
     */
    @Override
    public String getNazev() {
        return NAZEV;
    }

}
