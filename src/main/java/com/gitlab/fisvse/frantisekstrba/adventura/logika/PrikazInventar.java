package com.gitlab.fisvse.frantisekstrba.adventura.logika;

/**
 * Třída PrikazInventar realizuje vypsání obsahu Inventáře.
 * <p>
 * Tato třída je součástí jednoduché textové hry.
 *
 * @author Frantisek Strba
 * @version 1.0
 * @created ZS 2018
 */
public class PrikazInventar implements IPrikaz {

    private static final String NAZEV = "inventar";
    private final HerniPlan plan;

    public PrikazInventar(HerniPlan plan) {
        this.plan = plan;
    }

    @Override
    public String provedPrikaz(String... parametry) {
        return plan.getInventar().obsahInventare();
    }

    @Override
    public String getNazev() {
        return NAZEV;
    }

}
