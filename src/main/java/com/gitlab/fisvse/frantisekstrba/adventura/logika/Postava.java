package com.gitlab.fisvse.frantisekstrba.adventura.logika;

/**
 * Třída Postava realizuje určitou postavu ve hře.
 * <p>
 * Tato třída je součástí jednoduché textové hry.
 *
 * @author Frantisek Strba
 * @version 1.0
 * @created ZS 2018
 */
public class Postava {

    private final String jmeno;
    private boolean dead;

    /**
     * Jméno a zjištění, zda je postava mrtvá.
     *
     * @param jmeno Jméno postavy
     */
    public Postava(String jmeno) {
        this.jmeno = jmeno;
        dead = false;
    }

    public String getJmeno() {
        return jmeno;
    }

    boolean isDead() {
        return dead;
    }

    void setDead(boolean dead) {
        this.dead = dead;
    }
}
