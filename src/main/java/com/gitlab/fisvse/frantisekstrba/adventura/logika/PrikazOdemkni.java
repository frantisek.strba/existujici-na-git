package com.gitlab.fisvse.frantisekstrba.adventura.logika;

/**
 * Třída PrikazOdemkni realizuje příkaz odemkni.
 * Tato třída odemyká zamčené místnosti
 * <p>
 * Tato třída je součástí jednoduché textové hry.
 *
 * @author Frantisek Strba
 * @version 1.0
 * @created ZS 2018
 */
public class PrikazOdemkni implements IPrikaz {

    private static final String NAZEV = "odemkni";
    private final HerniPlan plan;

    /**
     * Konstruktor třídy
     *
     * @param plan herní plán musí být k dispocizi, abychom
     *             věděli, jakou místnost je nutno odemknout.
     */
    public PrikazOdemkni(HerniPlan plan) {
        this.plan = plan;

    }

    /**
     * Realizuje příkaz, kterým odemkne určitou místnost. Pokud je vedlejší
     * místnost neviditelná nebo neexistuje, vypíše se, že do místnosti nevedou
     * dveře. Pokud je zamčená a máme klíč, odemkne místnost. Pokud místnost
     * byla již odemknuta, vypíše, že již není třeba odemykat.
     *
     * @param parametry -
     * @return zpráva, kterou vypíše hra hráči
     */
    @Override
    public String provedPrikaz(String... parametry) {
        if (parametry.length == 0) {
            return "Co mám odemknout? Musíš zadat jméno místnosti";
        }

        String prostor = parametry[0];

        Prostor sousedniMistnost = plan.getAktualniProstor().vratSousedniProstor(prostor);
        Inventar inventar = plan.getInventar();

        // sousedni místnost neexistuje, nebo je zatím neviditelná
        if (sousedniMistnost == null || !sousedniMistnost.isVisible()) {
            return "Odsud nevedou dveře do místnosti " + prostor + " !";
        } else {
            if (sousedniMistnost.isZamcena()) {

                if (inventar.obsahujeVec(sousedniMistnost.getKlic().getJmeno())) {
                    //máme klíč a dveře jsou zamčené
                    sousedniMistnost.setZamcena(false);
                    return "Odemknul si dveře do místnosti "
                            + prostor + ". Můžeš volně projít";
                } else {
                    //nemáme klíč
                    return "Pro odemčení dveří do " + prostor + " u sebe "
                            + "potřebuješ mít "
                            + sousedniMistnost.getKlic().getJmeno();
                }
            } else {
                // již odemčená
                return "Místnost " + prostor + " již není třeba odemykat!";
            }
        }
    }

    /**
     * Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *
     * @return
     * @ return nazev prikazu
     */
    @Override
    public String getNazev() {
        return NAZEV;
    }
}
