package com.gitlab.fisvse.frantisekstrba.adventura.logika;

/**
 * Třída PrikazSeber realizuje příkaz seber.
 * <p>
 * Tato třída je součástí jednoduché textové hry.
 *
 * @author Frantisek Strba
 * @version 1.0
 * @created ZS 2018
 */
public class PrikazSeber implements IPrikaz {

    private static final String NAZEV = "seber";
    private final HerniPlan plan;

    public PrikazSeber(HerniPlan plan) {
        this.plan = plan;

    }

    /**
     * Provádí příkaz "seber". Zkouší se sebrat různé věci. Přenosné věci se
     * přidají do batohu, pokud není plný.
     *
     * @param parametry - jako parametr obsahuje jméno věci, která se má sebrat.
     * @return zpráva, kterou vypíše hra hráči
     */
    @Override
    public String provedPrikaz(String... parametry) {
        if (parametry.length == 0) {
            return "Co mám sebrat? Zadej jméno věci.";
        }
        String jmeno = parametry[0];
        Prostor aktualniProstor = plan.getAktualniProstor();

        if (aktualniProstor.obsahujeVec(jmeno)) {
            Vec vec = aktualniProstor.vyberVec(jmeno);
            if (vec == null) {
                return "Tato věc není přenosná.";
            } else {
                Inventar inventar = plan.getInventar();
                if (inventar.vlozVecDoInventare(vec)) {
                    aktualniProstor.odeberVec(vec.getJmeno());
                    return "Sebral jsi " + vec.getJmeno();
                } else {
                    aktualniProstor.vlozVec(vec);
                    return "To se ti už do inventáře nevejde. Něco zahoď!";
                }
            }
        } else {
            return "Nic takového tu není.";
        }
    }

    /**
     * Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *
     * @ return nazev prikazu
     */
    @Override
    public String getNazev() {
        return NAZEV;
    }
}
