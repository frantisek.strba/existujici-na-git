package com.gitlab.fisvse.frantisekstrba.adventura.logika;

/**
 * Třída PrikazProzkoumej realizuje příkaz prozkoumej.
 * <p>
 * Tato třída je součástí jednoduché textové hry.
 *
 * @author Frantisek Strba
 * @version 1.0
 * @created ZS 2018
 */
public class PrikazProzkoumej implements IPrikaz {

    private static final String NAZEV = "prozkoumej";
    private final HerniPlan plan;


    /*
     * Konstruktor třídy PrikazProzkoumej
     *
     * @param plan
     */
    public PrikazProzkoumej(HerniPlan plan) {
        this.plan = plan;

    }

    /**
     * Provádí příkaz "prozkoumej". Pokud je věc otevřená, vrátí svůj popis.
     *
     * @param parametry - jako parametr obsahuje věc, která se má prozkoumat.
     * @return zpráva, kterou vypíše hra hráči
     */
    @Override
    public String provedPrikaz(String... parametry) {
        if (parametry.length == 0) {
            return "Co mám prozkoumat? Musíš zadat jméno věci";
        }

        String jmeno = parametry[0];
        Prostor aktMist = plan.getAktualniProstor();

        if (aktMist.obsahujeVec(jmeno)) {
            Vec vec = aktMist.getVec(jmeno);
            if (vec == null) {
                return "Nic takového tu není.";
            } else {

                if (vec.isOtevrena()) {
                    // Pokud je věc otevřená, musíme vědět, že byla
                    // prozkoumána, abychom z ní mohli brát další věci
                    vec.setProzkoumana(true);
                    return vec.popisProzkoumej();
                }
                return "Věc je zamčená. Zkus použít příkaz 'otevri'";
            }

        }
        // prozkoumat i postavy???

        // Prozkoumá i věci v inventáři
//        Inventar inventar = plan.getInventar();
//        if (inventar.obsahujeVec(jmeno)) {
//            return inventar.getVec(jmeno).popisProzkoumej();
//        }

        return "Taková věc tu není ";
    }

    /**
     * Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *
     * @ return nazev prikazu
     */
    @Override
    public String getNazev() {
        return NAZEV;
    }
}
