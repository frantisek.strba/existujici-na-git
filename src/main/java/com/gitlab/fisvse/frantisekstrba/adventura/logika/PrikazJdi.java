package com.gitlab.fisvse.frantisekstrba.adventura.logika;

/**
 * Třída PrikazJdi implementuje pro hru příkaz jdi
 * <p>
 * Tato třída je součástí jednoduché textové hry.
 *
 * @author Frantisek Strba
 * @version 1.0
 * @created ZS 2018
 */
public class PrikazJdi implements IPrikaz {

    private static final String NAZEV = "jdi";
    private final HerniPlan plan;
    private final Hra hra;

    /**
     * Konstruktor třídy
     *
     * @param plan herní plán, ve kterém se bude ve hře "chodit"
     */
    public PrikazJdi(HerniPlan plan, Hra hra) {
        this.plan = plan;
        this.hra = hra;
    }

    public static String getNazevStatic() {
        return NAZEV;
    }

    /**
     * Provádí příkaz "jdi". Zkouší se vyjít do zadaného prostoru. Pokud prostor
     * existuje, vstoupí se do nového prostoru. Pokud zadaný sousední prostor
     * (východ) není, vypíše se chybové hlášení.
     *
     * @param parametry - jako parametr obsahuje jméno prostoru (východu), do
     *                  kterého se má jít.
     * @return zpráva, kterou vypíše hra hráči
     */
    @Override
    public String provedPrikaz(String... parametry) {
        if (parametry.length == 0) {
            // pokud chybí druhé slovo (sousední prostor), tak ....
            return "Kam mám jít? Musíš zadat jméno východu";
        }

        String smer = parametry[0];

        Prostor sousedniProstor = plan.getAktualniProstor().vratSousedniProstor(smer);
        Inventar inventar = plan.getInventar();

        // zkoušíme přejít do sousedního prostoru
        if (sousedniProstor == null || !sousedniProstor.isVisible()) {
            return "Tam se jít nedá!";

            // Mimozemšťan nás zabije, pokud nemáme zbraň
        } else if (sousedniProstor.getNazev().equals("cesta_ven") && !inventar.obsahujeVec("pistol")) {
            hra.setKonecHry(true);
            return "Nebyl si dostatečně připraven. Mimozemšťan tě zabil. Hra končí.";

            // V prostoru exit hra končí. Výhra
        } else if (sousedniProstor.getNazev().equals("exit")) {
            hra.setKonecHry(true);
            return "Dostal jsi se z lodi. Vyhrál jsi.";
        } else {
            // sousední prostor je zamčený
            if (sousedniProstor.isZamcena()) {
                return "dveře do místnosti " + sousedniProstor.getNazev()
                        + " jsou zamčené";
            }
            plan.setAktualniProstor(sousedniProstor);
            return sousedniProstor.dlouhyPopis();
        }
    }

    /**
     * Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *
     * @ return nazev prikazu
     */
    @Override
    public String getNazev() {
        return NAZEV;
    }

}
