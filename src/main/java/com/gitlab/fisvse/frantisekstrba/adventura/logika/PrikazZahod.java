package com.gitlab.fisvse.frantisekstrba.adventura.logika;

/**
 * Třída Prikaz zahoď realizuje příkaz "zahoď"
 * Pokud je věc v inventáři, zahodí ji v aktuální místnosti na zem
 * <p>
 * Tato třída je součástí jednoduché textové hry.
 *
 * @author Frantisek Strba
 * @version 1.0
 * @created ZS 2018
 */
public class PrikazZahod implements IPrikaz {

    private static final String NAZEV = "zahod";
    private final HerniPlan plan;

    public PrikazZahod(HerniPlan plan) {
        this.plan = plan;
    }

    @Override
    public String provedPrikaz(String... parametry) {
        if (parametry.length == 0) {
            return "Co mám zahodit? Zadej druhý paramtr";
        }
        String nazevVeci = parametry[0];
        Inventar inventar = plan.getInventar();

        if (inventar.obsahujeVec(nazevVeci)) {
            Vec vec = inventar.getVec(nazevVeci);
            inventar.odeberVec(nazevVeci);
            plan.getAktualniProstor().vlozVec(vec);
            return "Věc: " + nazevVeci + " byla odhozena na zem";
        }
        return "Tuto věc v inventáři nemáš.";
    }

    @Override
    public String getNazev() {
        return NAZEV;
    }

}
