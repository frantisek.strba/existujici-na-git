package com.gitlab.fisvse.frantisekstrba.adventura.logika;

/**
 * Třída PrikazPouzij realizuje příkaz použij. Pokud je to možné, tak na určitém
 * místě hráč použije item, který má k dispozici v inventáři
 * <p>
 * Tato třída je součástí jednoduché textové hry.
 *
 * @author Frantisek Strba
 * @version 1.0
 * @created ZS 2018
 */
public class PrikazPouzij implements IPrikaz {

    private static final String NAZEV = "pouzij";
    private final HerniPlan plan;

    /**
     * Konstruktor třídy
     *
     * @param plan herní plán, abychom věděli, kde je možné item použít.
     */
    public PrikazPouzij(HerniPlan plan) {
        this.plan = plan;

    }

    /**
     * Provádí příkaz "pouzij". Zjišťuje, zda je možné v aktuální místnosti
     * použít určitý item. Pokud to možné je, použije item. V opačném případě
     * dostane hráč oznámení, že item v inventáři nemá, nebo nelze použít.
     *
     * @param parametry - jako parametr obsahuje jméno místnosti, do které se má
     *                  jít.
     * @return zpráva, kterou vypíše hra hráči
     */
    @Override
    public String provedPrikaz(String... parametry) {
        String nazev = parametry[0];
        // 
        if (parametry.length == 0) {
            return "Co mám použít ....";
        }
        Prostor aktProstor = plan.getAktualniProstor();
        Inventar inventar = plan.getInventar();
        if (nazev.equals("baterka") && aktProstor.getNazev().equals("temna_mistnost") && inventar.obsahujeVec("baterka")) {
            // Po použití baterky se zobrazí dva prostory (viz. mapa)
            plan.setProstorVisible(plan.getProstorByName("sklad_zbrane"));
            plan.setProstorVisible(plan.getProstorByName("chodba_leva"));
            return "Použil si baterku, teď máš přístup do dalších místností.\n" + plan.getAktualniProstor().popisVychodu();

        } else if (nazev.equals("pistol") && aktProstor.getNazev().equals("cesta_ven") && inventar.obsahujeVec("pistol")) {
            // Pokud máme zbraň, můžeme mimozemšťana zabít.    
            Postava mimozemstan = aktProstor.getPostava("mimozemstan");
            if (mimozemstan.isDead()) {
                return "Mimozemšťan je již mrtvý";
            } else {
                mimozemstan.setDead(true);
                // Po zabití můžeme opustit loď 
                plan.setProstorVisible(plan.getProstorByName("exit"));
                return "Zabil jsi mimozemšťana a můžeš tak opustit vesmírnou loď. \n" + plan.getAktualniProstor().popisVychodu();
            }
        } else {
            return "Tento předmět nemáš v inventáři, nebo na tomto místě nelze použít";
        }
    }

    /**
     * Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *
     * @return
     * @ return nazev prikazu
     */
    @Override
    public String getNazev() {
        return NAZEV;
    }
}
