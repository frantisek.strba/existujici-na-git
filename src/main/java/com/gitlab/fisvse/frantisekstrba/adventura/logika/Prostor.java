package com.gitlab.fisvse.frantisekstrba.adventura.logika;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Třída Prostor reprezentuje místnost ve hře.
 * <p>
 * Tato třída je součástí jednoduché textové hry.
 *
 * @author Frantisek Strba
 * @version 1.0
 * @created ZS 2018
 */
public class Prostor {

    private final String nazev;
    private final String popis;
    private final Set<Prostor> vychody;   // obsahuje sousední místnosti
    private final Map<String, Vec> veciVProstoru;
    private Map<String, Postava> seznamPostav = new HashMap<>();
    private boolean zamcena;
    private Vec klic;
    private boolean visible;

    /**
     * Vytvoření prostoru se zadaným popisem
     *
     * @param nazev nazev prostoru, jednoznačný identifikátor, jedno slovo nebo
     *              víceslovný název bez mezer.
     * @param popis Popis prostoru.
     */
    public Prostor(String nazev, String popis) {
        this.nazev = nazev;
        this.popis = popis;
        vychody = new HashSet<>();
        veciVProstoru = new HashMap<>();
        zamcena = false;
        visible = true;
        this.seznamPostav = new HashMap<>();
    }

    /**
     * Definuje východ z prostoru (sousední/vedlejsi prostor). Vzhledem k tomu,
     * že je použit Set pro uložení východů, může být sousední prostor uveden
     * pouze jednou (tj. nelze mít dvoje dveře do stejné sousední místnosti).
     * Druhé zadání stejného prostoru tiše přepíše předchozí zadání (neobjeví se
     * žádné chybové hlášení). Lze zadat též cestu ze do sebe sama.
     *
     * @param vedlejsi prostor, který sousedi s aktualnim prostorem.
     */
    public void setVychod(Prostor vedlejsi) {
        vychody.add(vedlejsi);
    }

    /**
     * Metoda equals pro porovnání dvou prostorů. Překrývá se metoda equals ze
     * třídy Object. Dva prostory jsou shodné, pokud mají stejný název. Tato
     * metoda je důležitá z hlediska správného fungování seznamu východů (Set).
     * <p>
     * Bližší popis metody equals je u třídy Object.
     *
     * @param o object, který se má porovnávat s aktuálním
     * @return hodnotu true, pokud má zadaný prostor stejný název, jinak false
     */
    @Override
    public boolean equals(Object o) {
        // porovnáváme zda se nejedná o dva odkazy na stejnou instanci
        if (this == o) {
            return true;
        }
        // porovnáváme jakého typu je parametr 
        if (!(o instanceof Prostor)) {
            return false;    // pokud parametr není typu Prostor, vrátíme false
        }
        // přetypujeme parametr na typ Prostor 
        Prostor druhy = (Prostor) o;

        //metoda equals třídy java.util.Objects porovná hodnoty obou názvů. 
        //Vrátí true pro stejné názvy a i v případě, že jsou oba názvy null,
        //jinak vrátí false.
        return (java.util.Objects.equals(this.nazev, druhy.nazev));
    }

    /**
     * metoda hashCode vraci ciselny identifikator instance, ktery se pouziva
     * pro optimalizaci ukladani v dynamickych datovych strukturach. Pri
     * prekryti metody equals je potreba prekryt i metodu hashCode. Podrobny
     * popis pravidel pro vytvareni metody hashCode je u metody hashCode ve
     * tride Object
     *
     * @return
     */
    @Override
    public int hashCode() {
        int vysledek = 3;
        int hashNazvu = java.util.Objects.hashCode(this.nazev);
        vysledek = 37 * vysledek + hashNazvu;
        return vysledek;
    }

    /**
     * Vrací název prostoru (byl zadán při vytváření prostoru jako parametr
     * konstruktoru)
     *
     * @return název prostoru
     */
    public String getNazev() {
        return nazev;
    }

    /**
     * Vrací "dlouhý" popis prostoru.
     *
     * @return Dlouhý popis prostoru
     */
    public String dlouhyPopis() {
        return "Jsi v prostoru: " + popis + ".\n"
                + popisVychodu();
    }

    /**
     * Vrací textový řetězec, který popisuje sousední východy, například:
     * "vychody: ridici_mistnost ".
     *
     * @return Popis východů - názvů sousedních prostorů
     */
    public String popisVychodu() {
        String vracenyText = "východy:";
        for (Prostor sousedni : vychody) {
            if (sousedni.isVisible()) {
                vracenyText += " " + sousedni.getNazev();
                if (sousedni.isZamcena()) {
                    vracenyText += "(zamknuto)";
                }
            }

        }
        return vracenyText;
    }

    /**
     * Vrací prostor, který sousedí s aktuálním prostorem a jehož název je zadán
     * jako parametr. Pokud prostor s udaným jménem nesousedí s aktuálním
     * prostorem, vrací se hodnota null.
     *
     * @param nazevSouseda Jméno sousedního prostoru (východu)
     * @return Prostor, který se nachází za příslušným východem, nebo hodnota
     * null, pokud prostor zadaného jména není sousedem.
     */
    public Prostor vratSousedniProstor(String nazevSouseda) {
        List<Prostor> hledaneProstory
                = vychody.stream()
                .filter(sousedni -> sousedni.getNazev().equals(nazevSouseda))
                .collect(Collectors.toList());
        if (hledaneProstory.isEmpty()) {
            return null;
        } else {
            return hledaneProstory.get(0);
        }
    }

    /**
     * Vrací kolekci obsahující prostory, se kterými tento prostor sousedí.
     * Takto získaný seznam sousedních prostor nelze upravovat (přidávat,
     * odebírat východy) protože z hlediska správného návrhu je to plně
     * záležitostí třídy Prostor.
     *
     * @return Nemodifikovatelná kolekce prostorů (východů), se kterými tento
     * prostor sousedí.
     */
    public Collection<Prostor> getVychody() {
        return Collections.unmodifiableCollection(vychody);
    }

    /**
     * Zjistí, zda se určitá věc nachází v místnosti.
     *
     * @param jmeno Název věci.
     * @return True pokud se nachází. False pokud nikoliv.
     */
    public boolean obsahujeVec(String jmeno) {

        for (String nazevVeci : veciVProstoru.keySet()) {

            if (nazevVeci.equals(jmeno)) {
                return true;
            }
            if ((veciVProstoru.get(nazevVeci)).obsahujeVec(jmeno)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Vybere věc v prostoru.
     *
     * @param jmeno Název věci.
     * @return True pokud se nachází. False pokud nikoliv.
     */
    public Vec vyberVec(String jmeno) {
        Vec vec = null;
        for (String nazevVeci : veciVProstoru.keySet()) {
            if (nazevVeci.equals(jmeno) && veciVProstoru.get(nazevVeci).isPrenositelna()) {
                vec = veciVProstoru.get(nazevVeci);
                break;
            }
            Vec vnejsi = veciVProstoru.get(nazevVeci);
            if (vnejsi.obsahujeVec(jmeno)) {
                vec = vnejsi.vyberVec(jmeno);
                break;
            }
        }

        return vec;

    }

    /**
     * Metoda vloží věc do prostoru.
     *
     * @param vec Věc ke vložení
     */
    public void vlozVec(Vec vec) {
        veciVProstoru.put(vec.getJmeno(), vec);
    }

    /**
     * Metoda odebere věc z prostoru.
     *
     * @param nazev Název věci
     * @return
     */
    public Vec odeberVec(String nazev) {
        return veciVProstoru.remove(nazev);
    }

    public Vec getVec(String nazev) {
        return veciVProstoru.get(nazev);
    }

    /**
     * Metodá vrátí text s věcmi v místnosti.
     *
     * @return
     */
    String predmetyVMistnosti() {
        if (veciVProstoru.isEmpty()) {
            return "V místnosti nejsou žádné předměty.";
        }
        String predmety = "Předměty v místnosti: ";
        predmety = veciVProstoru.entrySet().stream().map((entry) -> entry.getKey() + " ").reduce(predmety, String::concat);
        return predmety;
    }

    public boolean isZamcena() {
        return zamcena;
    }

    public void setZamcena(boolean zamcena) {
        this.zamcena = zamcena;
    }

    public void nastavKlic(Vec klic) {
        this.klic = klic;
    }

    public Vec getKlic() {
        return klic;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public void vlozPostavu(Postava postava) {
        seznamPostav.put(postava.getJmeno(), postava);
    }

    public Postava getPostava(String nazevPostavy) {
        return seznamPostav.get(nazevPostavy);
    }

    @Override
    public String toString() {
        return getNazev();
    }

}
