package com.gitlab.fisvse.frantisekstrba.adventura.main;

import com.gitlab.fisvse.frantisekstrba.adventura.logika.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Point2D;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import sun.applet.Main;

import java.util.HashMap;
import java.util.Map;


public class MainController implements Observer {

    @FXML
    public TextField vstup;
    @FXML
    public TextArea vystup;
    @FXML
    public Button odesli;
    @FXML
    public ImageView hrac;
    @FXML
    public MenuItem pomoc;
    @FXML
    public ListView batohListView;
    @FXML
    public ListView seznamVychodu;

    private Map<String, Point2D> souradniceProstoru;

    private IHra hra;

    public void odeslaniPrikazu(ActionEvent actionEvent) {
        String vstupText = vstup.getText();
        if (!vstupText.equals("")) {
            zpracovaniPrikazu(vstupText);
            vstup.setText("");
        }
        isKonecHry();
    }

    /**
     * Tlačítko, které spustí novou hru
     *
     * @param actionEvent
     */
    public void novaHra(ActionEvent actionEvent) {
        IHra hra = new Hra();

        inicializuj(hra);

    }

    /**
     * Zobrazí dialogové okno s návodem hry
     *
     * @param actionEvent
     */
    public void zobrazPomoc(ActionEvent actionEvent) {
        Stage primaryStage = new Stage();
        primaryStage.setTitle("Nápověda");

        WebView webView = new WebView();

        webView.getEngine().load(Main.class.getResource("/napoveda.html").toExternalForm());
        VBox vBox = new VBox(webView);
        Scene scene = new Scene(vBox, 960, 600);

        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * Tlačítko, které ukončí hru
     */
    public void konecHry() {
        System.exit(0);
    }

    /**
     * Zpracuje příkaz zadaný uživatelem a poté aktualizuje hru
     *
     * @param prikaz
     */
    private void zpracovaniPrikazu(String prikaz) {
        String odpoved = hra.zpracujPrikaz(prikaz);
        vystup.appendText("\n\nPříkaz: " + prikaz + "\n");
        vystup.appendText(odpoved + "\n");
        update();
        isKonecHry();
    }

    /**
     * Inicializuje hru, vytvoří mapu souřadnic a nastaví okna
     *
     * @param hra
     */
    public void inicializuj(IHra hra) {
        vystup.clear();
        vstup.clear();
        vstup.setDisable(false);
        seznamVychodu.setDisable(false);

        this.hra = hra;
        vystup.appendText(hra.vratUvitani());
        vystup.setEditable(false);
        vstup.requestFocus();
        vystup.setWrapText(true);

        vytvorMapuSouradnic();
        hra.getHerniPlan().register(this);
        update();
    }

    /**
     * Vytvoří mapu, která obsahuje místnost a její pozici v mapě
     */
    private void vytvorMapuSouradnic() {
        souradniceProstoru = new HashMap<>();

        souradniceProstoru.put("ridici_mistnost", new Point2D(164, 162));
        souradniceProstoru.put("spolecenska_mistnost", new Point2D(164, 93));
        souradniceProstoru.put("chodba_A", new Point2D(98, 93));
        souradniceProstoru.put("chodba_B", new Point2D(233, 93));
        souradniceProstoru.put("temna_mistnost", new Point2D(45, 93));
        souradniceProstoru.put("mistnost_trezor", new Point2D(283, 93));
        souradniceProstoru.put("sklad_zasob", new Point2D(281, 155));
        souradniceProstoru.put("sklad_zbrane", new Point2D(46, 155));
        souradniceProstoru.put("chodba_leva", new Point2D(46, 23));
        souradniceProstoru.put("chodba_prava", new Point2D(280, 23));
        souradniceProstoru.put("cesta_ven", new Point2D(164, 29));
    }

    /**
     * Podle pozice hráče posune obrázek v mapě a podle obsahu inventáře zobrazí obrázky do panelu
     */
    @Override
    public void update() {
        Prostor aktualniProstor = hra.getHerniPlan().getAktualniProstor();

        ObservableList items = seznamVychodu.getItems();
        items.clear();
        items.addAll(aktualniProstor.getVychody());

        hrac.setX(souradniceProstoru.get(aktualniProstor.getNazev()).getX());
        hrac.setY(souradniceProstoru.get(aktualniProstor.getNazev()).getY());

        Inventar inventar = hra.getHerniPlan().getInventar();
        Map<String, Vec> batoh = inventar.getVeci();
        ObservableList<Vec> obsahbatohu = FXCollections.observableArrayList();

        for (Map.Entry<String, Vec> entry : batoh.entrySet()) {
            obsahbatohu.add(entry.getValue());
        }

        batohListView.setItems(obsahbatohu);

    }

    /**
     * Pokud je konec hry, tak se zobrazí epilog a hra se zablokuje
     */
    private void isKonecHry() {
        if (hra.konecHry()) {
            vstup.setDisable(true);
            odesli.setDisable(true);
            seznamVychodu.setDisable(true);
            vystup.appendText("\n\n" + hra.vratEpilog());
        }
    }

    /**
     * Při kliknutí na sousední prostor se tam hráč přesune a hra se aktualizuje.
     *
     * @param mouseEvent
     */
    public void pruchod(MouseEvent mouseEvent) {
        Prostor prostor = (Prostor) seznamVychodu.getSelectionModel().getSelectedItem();
        String prikaz = PrikazJdi.getNazevStatic() + Hra.getOddelovac() + prostor;
        zpracovaniPrikazu(prikaz);
    }
}
