package com.gitlab.fisvse.frantisekstrba.adventura.logika;

/**
 * Třída Hra - třída představující logiku adventury.
 * <p>
 * Toto je hlavní třída logiky aplikace. Tato třída vytváří instanci třídy
 * HerniPlan, která inicializuje mistnosti hry a vytváří seznam platných příkazů
 * a instance tříd provádějící jednotlivé příkazy. Vypisuje uvítací a ukončovací
 * text hry. Také vyhodnocuje jednotlivé příkazy zadané uživatelem.
 */
public class Hra implements IHra {

    private static final String ODDELOVAC = " ";
    private final SeznamPrikazu platnePrikazy;    // obsahuje seznam přípustných příkazů
    private final HerniPlan herniPlan;
    private boolean konecHry = false;

    /**
     * Vytváří hru a inicializuje místnosti (prostřednictvím třídy HerniPlan) a
     * seznam platných příkazů.
     */
    public Hra() {
        herniPlan = new HerniPlan();
        platnePrikazy = new SeznamPrikazu();
        platnePrikazy.vlozPrikaz(new PrikazNapoveda(platnePrikazy));
        platnePrikazy.vlozPrikaz(new PrikazJdi(herniPlan, this));
        platnePrikazy.vlozPrikaz(new PrikazKonec(this));
        platnePrikazy.vlozPrikaz(new PrikazInventar(herniPlan));
        platnePrikazy.vlozPrikaz(new PrikazRozhledniSe(herniPlan));
        platnePrikazy.vlozPrikaz(new PrikazSeber(herniPlan));
        platnePrikazy.vlozPrikaz(new PrikazZahod(herniPlan));
        platnePrikazy.vlozPrikaz(new PrikazOdemkni(herniPlan));
        platnePrikazy.vlozPrikaz(new PrikazOtevri(herniPlan));
        platnePrikazy.vlozPrikaz(new PrikazProzkoumej(herniPlan));
        platnePrikazy.vlozPrikaz(new PrikazPouzij(herniPlan));

    }

    public static String getOddelovac() {
        return ODDELOVAC;
    }

    /**
     * Vrátí úvodní zprávu pro hráče.
     */
    @Override
    public String vratUvitani() {
        return "Vítej ve hře!\n"
                + "Byl jsi vyslán na nebezpečnou misi na cizí planetu.\n"
                + "Cestuješ ve vesmírné lodi, ale těsně před přistáním tě zasáhnou mimozemšťané a tvá loď havaruje.\n"
                + "Všichni kromě tebe jsou mrtví a tvá mise nezačíná dobře.\n"
                + "Nacházíš se v řídící místnosti a tvým úkolem je dostat se pryč z lodi.\n"
                + "Napiš 'napoveda', pokud si nevíš rady, jak hrát dál.\n"
                + "\n"
                + herniPlan.getAktualniProstor().dlouhyPopis();
    }

    /**
     * Vrátí závěrečnou zprávu pro hráče.
     */
    @Override
    public String vratEpilog() {
        return "Dík, že jste si zahráli. Ahoj.";
    }

    /**
     * Vrací true, pokud hra skončila.
     */
    @Override
    public boolean konecHry() {
        return konecHry;
    }

    /**
     * Metoda zpracuje řetězec uvedený jako parametr, rozdělí ho na slovo
     * příkazu a další parametry. Pak otestuje zda příkaz je klíčovým slovem
     * např. jdi. Pokud ano spustí samotné provádění příkazu.
     *
     * @param radek text, který zadal uživatel jako příkaz do hry.
     * @return vrací se řetězec, který se má vypsat na obrazovku
     */
    @Override
    public String zpracujPrikaz(String radek) {
        String[] slova = radek.split("[ \t]+");
        String slovoPrikazu = slova[0];
        String[] parametry = new String[slova.length - 1];
        for (int i = 0; i < parametry.length; i++) {
            parametry[i] = slova[i + 1];
        }
        String textKVypsani;
        if (platnePrikazy.jePlatnyPrikaz(slovoPrikazu)) {
            IPrikaz prikaz = platnePrikazy.vratPrikaz(slovoPrikazu);
            textKVypsani = prikaz.provedPrikaz(parametry);
        } else {
            textKVypsani = "Nevím co tím myslíš? Tento příkaz neznám. ";
        }
        return textKVypsani;
    }

    /**
     * Nastaví, že je konec hry, metodu využívá třída PrikazKonec, mohou ji
     * použít i další implementace rozhraní Prikaz.
     *
     * @param konecHry hodnota false= konec hry, true = hra pokračuje
     */
    void setKonecHry(boolean konecHry) {
        this.konecHry = konecHry;
    }

    /**
     * Metoda vrátí odkaz na herní plán, je využita hlavně v testech, kde se
     * jejím prostřednictvím získává aktualní místnost hry.
     *
     * @return odkaz na herní plán
     */
    @Override
    public HerniPlan getHerniPlan() {
        return herniPlan;
    }


}
