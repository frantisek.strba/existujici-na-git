package com.gitlab.fisvse.frantisekstrba.adventura.logika;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Třída TextoveDotazy
 * <p>
 * Tato třída je součástí jednoduché textové hry.
 *
 * @author Frantisek Strba
 * @version 1.0
 * @created ZS 2018
 */
public class TextoveDotazy {


    public String odpovez(String otazka) {

        return prectiString(otazka);
    }


    /**
     * Metoda přečte řetězec z konzole. Parametrem je prompt, který se nejdříve vypíše
     * na konzoli.
     *
     * @return Vrací přečtený řetězec
     */
    private String prectiString(String otazka) {
        String vstupniRadek = "";
        System.out.println(otazka);        // vypíše se otázka

        BufferedReader vstup =
                new BufferedReader(new InputStreamReader(System.in));
        try {
            vstupniRadek = vstup.readLine();
        } catch (java.io.IOException exc) {
            System.out.println("Vyskytla se chyba během čtení z konzole. "
                    + exc.getMessage());
        }
        return vstupniRadek;
    }
}

