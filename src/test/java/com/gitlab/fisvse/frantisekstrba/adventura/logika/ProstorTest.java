package com.gitlab.fisvse.frantisekstrba.adventura.logika;

import org.junit.*;

import static org.junit.Assert.assertEquals;

/*
 * Testovací třída ProstorTest slouží ke komplexnímu otestování
 * třídy Prostor
 *
 * @author    František Štrba
 * @version   1.0
 * @created ZS 2018
 */
public class ProstorTest {

    private Prostor chodba;
    private Prostor pokoj;
    private Vec nuz;
    private Vec jablko;

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    /**
     * *************************************************************************
     * Metoda se provede před spuštěním každé testovací metody. Používá se k
     * vytvoření tzv. přípravku (fixture), což jsou datové atributy (objekty), s
     * nimiž budou testovací metody pracovat.
     */
    @Before
    public void setUp() {
        chodba = new Prostor("chodba", "Popis chodba");
        pokoj = new Prostor("pokoj", "Popis pokoj");
        nuz = new Vec("nuz", true, "Popis nuz");
        jablko = new Vec("jablko", true, "Popis jablko");
        chodba.setVychod(pokoj);
        pokoj.setVychod(chodba);
    }

    /**
     * *************************************************************************
     * Úklid po testu - tato metoda se spustí po vykonání každé testovací
     * metody.
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of popisVychodu method, of class Prostor.
     */
    @Test
    public void testPopisVychodu() {
        System.out.println("popisVychodu");

        assertEquals("východy: pokoj", chodba.popisVychodu());
        assertEquals("východy: chodba", pokoj.popisVychodu());
    }

    /**
     * Test of vratSousedniProstor method, of class Prostor.
     */
    @Test
    public void testVratSousedniProstor() {
        System.out.println("vratSousedniProstor");

        assertEquals(chodba, pokoj.vratSousedniProstor("chodba"));
        assertEquals(pokoj, chodba.vratSousedniProstor("pokoj"));
        assertEquals(null, chodba.vratSousedniProstor("asdf"));

    }

    /**
     * Test of obsahujeVec method, of class Prostor.
     */
    @Test
    public void testObsahujeVec() {
        System.out.println("obsahujeVec");

        // TODO nefunguje vnitřní předmět jablko
        Vec trezor = new Vec("trezor", false, "Popis trezor");
        chodba.vlozVec(nuz);
        trezor.vlozVec(jablko);
        chodba.vlozVec(trezor);
        trezor.setProzkoumana(true);
        assertEquals(true, chodba.obsahujeVec("nuz"));
        assertEquals(false, chodba.obsahujeVec("neco"));
        assertEquals(true, trezor.obsahujeVec("jablko"));
        assertEquals(true, chodba.obsahujeVec("jablko"));

    }

    /**
     * Test of vyberVec method, of class Prostor.
     */
    @Test
    public void testVyberVec() {
        System.out.println("vyberVec");
        Vec trezor = new Vec("trezor", false, "Popis trezor");
        chodba.vlozVec(nuz);
        trezor.vlozVec(jablko);
        chodba.vlozVec(trezor);
        trezor.setProzkoumana(true);
        assertEquals(nuz, chodba.vyberVec("nuz"));
        assertEquals(null, chodba.vyberVec("neco"));
        assertEquals(jablko, trezor.vyberVec("jablko"));
        assertEquals(null, chodba.vyberVec("jablko"));

    }

    /**
     * Test of predmetyVMistnosti method, of class Prostor.
     */
    @Test
    public void testPredmetyVMistnosti() {
        System.out.println("predmetyVMistnosti");
        chodba.vlozVec(nuz);
        chodba.vlozVec(jablko);
        assertEquals("V místnosti nejsou žádné předměty.", pokoj.predmetyVMistnosti());
        assertEquals("Předměty v místnosti: nuz jablko ", chodba.predmetyVMistnosti());
    }

}
