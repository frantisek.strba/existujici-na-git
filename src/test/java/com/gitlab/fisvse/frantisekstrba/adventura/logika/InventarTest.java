package com.gitlab.fisvse.frantisekstrba.adventura.logika;

import org.junit.*;

import static org.junit.Assert.assertEquals;

/*
 * Testovací třída ProstorTest slouží ke komplexnímu otestování
 * třídy Prostor
 *
 * @author    František Štrba
 * @version   1.0
 * @created ZS 2018
 */
public class InventarTest {

    private Inventar inventar;
    private Vec jablko;
    private Vec nuz;

    public InventarTest() {

    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        inventar = new Inventar();
        nuz = new Vec("nuz", true, "Popis nuz");
        jablko = new Vec("jablko", true, "Popis jablko");
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of vlozVecDoInventare method, of class Inventar.
     */
    @Test
    public void testVlozVecDoInventare() {
        System.out.println("vlozVecDoInventare");

        assertEquals(true, inventar.vlozVecDoInventare(nuz));

        assertEquals(true, inventar.vlozVecDoInventare(jablko));

        inventar.odeberVec(jablko.getJmeno());

        assertEquals(true, inventar.vlozVecDoInventare(jablko));
        assertEquals(true, inventar.vlozVecDoInventare(new Vec("cokoli", true, "popis cokoli")));
        assertEquals(false, inventar.vlozVecDoInventare(new Vec("jina", true, "popis jina")));

    }

    /**
     * Test of obsahInventare method, of class Inventar.
     */
    @Test
    public void testObsahInventare() {
        System.out.println("obsahInventare");
        assertEquals("Inventář je prázdný.", inventar.obsahInventare());
        inventar.vlozVecDoInventare(nuz);
        inventar.vlozVecDoInventare(jablko);
        assertEquals("Věci v inventáři: nuz jablko ", inventar.obsahInventare());

    }

}
