package com.gitlab.fisvse.frantisekstrba.adventura.logika;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/*
 * Testovací třída ProstorTest slouží ke komplexnímu otestování
 * třídy Prostor
 *
 * @author    František Štrba
 * @version   1.0
 * @created ZS 2018
 */
public class HraTest {

    private Hra hra;

    /**
     * *************************************************************************
     * Metoda se provede před spuštěním každé testovací metody. Používá se k
     * vytvoření tzv. přípravku (fixture), což jsou datové atributy (objekty), s
     * nimiž budou testovací metody pracovat.
     */
    @Before
    public void setUp() {
        hra = new Hra();
    }

    /**
     * *************************************************************************
     * Úklid po testu - tato metoda se spustí po vykonání každé testovací
     * metody.
     */
    @After
    public void tearDown() {
    }

    //== Soukromé metody používané v testovacích metodách ==========================
    //== Vlastní testovací metody ==================================================

    /**
     * *************************************************************************
     * Testuje průběh hry, po zavolání každěho příkazu testuje, zda hra končí a
     * v jaké aktuální místnosti se hráč nachází. Při dalším rozšiřování hry
     * doporučujeme testovat i jaké věci nebo osoby jsou v místnosti a jaké věci
     * jsou v batohu hráče.
     */
    @Test
    public void testPrubehHry() {
        assertEquals("ridici_mistnost", hra.getHerniPlan().getAktualniProstor().getNazev());
        hra.zpracujPrikaz("jdi spolecenska_mistnost");
        assertEquals(false, hra.konecHry());
        assertEquals("spolecenska_mistnost", hra.getHerniPlan().getAktualniProstor().getNazev());
        hra.zpracujPrikaz("jdi chodba_B");
        assertEquals(false, hra.konecHry());
        assertEquals("chodba_B", hra.getHerniPlan().getAktualniProstor().getNazev());
        hra.zpracujPrikaz("jdi mistnost_trezor");
        assertEquals(false, hra.konecHry());
        assertEquals("mistnost_trezor", hra.getHerniPlan().getAktualniProstor().getNazev());
        hra.zpracujPrikaz("otevri trezor");
        hra.zpracujPrikaz("prozkoumej trezor");
        hra.zpracujPrikaz("seber kapitansky_klic");

        assertEquals(true, hra.getHerniPlan().getInventar().obsahujeVec("kapitansky_klic"));
        assertEquals(true, hra.getHerniPlan().getAktualniProstor().getVec("trezor").isOtevrena());
        assertEquals(true, hra.getHerniPlan().getAktualniProstor().getVec("trezor").isProzkoumana());

        assertEquals(false, hra.konecHry());
        assertEquals("mistnost_trezor", hra.getHerniPlan().getAktualniProstor().getNazev());
        hra.zpracujPrikaz("jdi sklad_zasob");
        assertEquals(false, hra.konecHry());
        assertEquals("sklad_zasob", hra.getHerniPlan().getAktualniProstor().getNazev());
        hra.zpracujPrikaz("seber baterka");
        assertEquals(true, hra.getHerniPlan().getInventar().obsahujeVec("baterka"));
        hra.zpracujPrikaz("jdi mistnost_trezor");
        assertEquals(false, hra.konecHry());
        assertEquals("mistnost_trezor", hra.getHerniPlan().getAktualniProstor().getNazev());
        hra.zpracujPrikaz("jdi chodba_B");
        assertEquals(false, hra.konecHry());
        assertEquals("chodba_B", hra.getHerniPlan().getAktualniProstor().getNazev());
        hra.zpracujPrikaz("jdi spolecenska_mistnost");
        assertEquals(false, hra.konecHry());
        assertEquals("spolecenska_mistnost", hra.getHerniPlan().getAktualniProstor().getNazev());
        hra.zpracujPrikaz("jdi chodba_A");
        assertEquals(false, hra.konecHry());
        assertEquals("chodba_A", hra.getHerniPlan().getAktualniProstor().getNazev());
        hra.zpracujPrikaz("jdi temna_mistnost");
        assertEquals(false, hra.konecHry());
        assertEquals("temna_mistnost", hra.getHerniPlan().getAktualniProstor().getNazev());
        hra.zpracujPrikaz("pouzij baterka");
        assertEquals(true, hra.getHerniPlan().getProstorByName("sklad_zbrane").isVisible());
        assertEquals(true, hra.getHerniPlan().getProstorByName("chodba_leva").isVisible());
        hra.zpracujPrikaz("odemkni sklad_zbrane");
        assertEquals(false, hra.getHerniPlan().getProstorByName("sklad_zbrane").isZamcena());
        hra.zpracujPrikaz("jdi sklad_zbrane");
        assertEquals(false, hra.konecHry());
        assertEquals("sklad_zbrane", hra.getHerniPlan().getAktualniProstor().getNazev());
        hra.zpracujPrikaz("seber pistol");
        assertEquals(true, hra.getHerniPlan().getInventar().obsahujeVec("pistol"));
        hra.zpracujPrikaz("jdi temna_mistnost");
        assertEquals(false, hra.konecHry());
        assertEquals("temna_mistnost", hra.getHerniPlan().getAktualniProstor().getNazev());
        hra.zpracujPrikaz("jdi chodba_leva");
        assertEquals(false, hra.konecHry());
        assertEquals("chodba_leva", hra.getHerniPlan().getAktualniProstor().getNazev());
        hra.zpracujPrikaz("jdi cesta_ven");
        hra.zpracujPrikaz("pouzij pistol");
        assertEquals(true, hra.getHerniPlan().getProstorByName("exit").isVisible());
        assertEquals(false, hra.konecHry());
        assertEquals("cesta_ven", hra.getHerniPlan().getAktualniProstor().getNazev());
        hra.zpracujPrikaz("jdi exit");
        assertEquals(true, hra.konecHry());
    }

    @Test
    public void testSpatnyPrubeh() {
        assertEquals("ridici_mistnost", hra.getHerniPlan().getAktualniProstor().getNazev());
        hra.zpracujPrikaz("jdi spolecenska_mistnost");
        assertEquals(false, hra.konecHry());
        assertEquals("spolecenska_mistnost", hra.getHerniPlan().getAktualniProstor().getNazev());
        hra.zpracujPrikaz("jdi chodba_B");
        assertEquals(false, hra.konecHry());
        assertEquals("chodba_B", hra.getHerniPlan().getAktualniProstor().getNazev());
        hra.zpracujPrikaz("jdi mistnost_trezor");
        assertEquals(false, hra.konecHry());
        assertEquals("mistnost_trezor", hra.getHerniPlan().getAktualniProstor().getNazev());
        hra.zpracujPrikaz("jdi chodba_prava");
        assertEquals(false, hra.konecHry());
        assertEquals("chodba_prava", hra.getHerniPlan().getAktualniProstor().getNazev());
        hra.zpracujPrikaz("jdi cest_ven");
        assertEquals(true, hra.konecHry());
    }
}
