package com.gitlab.fisvse.frantisekstrba.adventura.logika;

import org.junit.*;

import static org.junit.Assert.assertEquals;

/**
 * @author Fox
 */
public class VecTest {

    public VecTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of vyberVec method, of class Vec.
     */
    @Test
    public void testVyberVec() {
        System.out.println("vyberVec");
        Vec trezor = new Vec("trezor", false, "Popis trezoru");
        Vec klic = new Vec("klic", true, "Popis klíče");
        trezor.vlozVec(klic);
        trezor.setProzkoumana(true);
        assertEquals(klic, trezor.vyberVec("klic"));

    }

    /**
     * Test of popisProzkoumej method, of class Vec.
     */
    @Test
    public void testPopisProzkoumej() {
        System.out.println("popisProzkoumej");
        Vec trezor = new Vec("trezor", false, "Popis trezoru");
        Vec klic = new Vec("klic", true, "Popis klíče");
        trezor.vlozVec(klic);
        assertEquals("Prohlédl jsi si důkladně " + trezor.getJmeno() + " a našel jsi: klic", trezor.popisProzkoumej());

    }

    @Test
    public void testObsahujeVec() {
        System.out.println("obsahujeVec");
        Vec vec1 = new Vec("a", false, "A");
        Vec vec2 = new Vec("b", true, "A");
        vec1.vlozVec(vec2);
        vec1.setProzkoumana(true);
        assertEquals(true, vec1.obsahujeVec("b"));
        assertEquals(false, vec1.obsahujeVec("c"));
    }

    /**
     * Test of getSeznamVeci method, of class Vec.
     */
    @Test
    public void testGetSeznamVeci() {
        System.out.println("getSeznamVeci");
        Vec trezor = new Vec("trezor", false, "Popis trezoru");
        Vec klic = new Vec("klic", true, "Popis klíče");
        trezor.vlozVec(klic);

        assertEquals(" klic ", trezor.getSeznamVeci());

    }
}
